package com.protei.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.protei.shared.Film;

import java.io.IOException;
import java.util.List;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("film")
public interface FilmService extends RemoteService {
  void newFilm(Film film) throws IOException;
  List<Film> getOld() throws IOException;
  int getSortType() throws IOException;
  void deleteFilm(int id) throws IOException;
  void sort(int sortType) throws IOException;
  void edit(Film film) throws IOException;
}
