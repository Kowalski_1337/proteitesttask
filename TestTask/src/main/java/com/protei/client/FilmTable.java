package com.protei.client;

import com.google.gwt.event.dom.client.HasAllDragAndDropHandlers;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasDoubleClickHandlers;
import com.protei.shared.Film;

import java.util.List;

public interface FilmTable extends HasAllDragAndDropHandlers, HasClickHandlers, HasDoubleClickHandlers {
    void addFilm(Film film);
    void deleteFilm(int index);
    void setFilm(int index, Film film);
    Film getFilm(int index);
    List<Film> getContent();
    void addFilms(List<Film> films);
}
