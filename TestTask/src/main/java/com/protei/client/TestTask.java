package com.protei.client;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;
import com.protei.shared.Film;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class TestTask implements EntryPoint {
    private final FilmServiceAsync filmService = GWT.create(FilmService.class);

    private final Button add = new Button("Add");
    private ListDataProvider<Film> listDataProvider = new ListDataProvider<>();
    private Set<String> names;
    private static final Comparator<Film> NEWEST = Comparator.comparing(Film::getId).reversed();
    private static final Comparator<Film> OlDEST = Comparator.comparing(Film::getId);
    private static final Comparator<Film> LEAST_LIKED = Comparator.comparing(Film::getRating);
    private static final Comparator<Film> MOST_LIKED = Comparator.comparing(Film::getRating).reversed();
    private static final Comparator<Film> NAME_UP = Comparator.comparing(Film::getName);
    private static final Comparator<Film> NAME_DOWN = Comparator.comparing(Film::getName).reversed();

    private Comparator<Film> comparator = OlDEST;
    /*private final FilmTable table;


    private final Consumer<Integer> deleteEvent = new Consumer<Integer>() {
        @Override
        public void accept(Integer index) {
            filmService.deleteFilm(index, new AsyncCallback<Integer>() {
                @Override
                public void onFailure(Throwable throwable) {
                    Window.alert("Cannot connect to server");
                }

                @Override
                public void onSuccess(Integer tablePos) {
                    table.deleteFilm(tablePos);
                }
            });
        }
    };

    TestTask(){
        table = new FilmTableImpl(deleteEvent);
    }*/

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        ListBox sortType = new ListBox();
        sortType.addItem("Newest");
        sortType.addItem("Oldest");
        sortType.addItem("Most liked");
        sortType.addItem("Least liked");
        sortType.addItem("Alphabetic up");
        sortType.addItem("Alphabetic down");

        filmService.getOld(new AsyncCallback<List<Film>>() {
            @Override
            public void onFailure(Throwable throwable) {
                names = new HashSet<>();
            }

            @Override
            public void onSuccess(List<Film> films) {
                names = films.stream().map(Film::getName).collect(Collectors.toSet());
                listDataProvider.getList().addAll(films);
            }
        });

        filmService.getSortType(new AsyncCallback<Integer>() {
            @Override
            public void onFailure(Throwable throwable) {

            }

            @Override
            public void onSuccess(Integer integer) {
                sortType.setItemSelected(integer, true);
                switch (integer) {
                    case Film.NEWEST:
                        comparator = NEWEST;
                        break;
                    case Film.OLDEST:
                        comparator = OlDEST;
                        break;
                    case Film.LEAST_LIKED:
                        comparator = LEAST_LIKED;
                        break;
                    case Film.MOST_LIKED:
                        comparator = MOST_LIKED;
                        break;
                    case Film.NAME_UP:
                        comparator = NAME_UP;
                        break;
                    case Film.NAME_DOWN:
                        comparator = NAME_DOWN;
                        break;

                }
            }
        });


        DialogBox addDialogBox = genDialogBox(null);

        add.addClickHandler(clickEvent -> {
            add.setEnabled(false);
            addDialogBox.center();
        });

        RootPanel.get("buttonsHandler").add(add);


        CellTable<Film> cellTable = new CellTable<>();
        cellTable.addColumn(new TextColumn<Film>() {
            @Override
            public String getValue(Film film) {
                return film.getName();
            }
        }, "Name");
        cellTable.addColumn(new TextColumn<Film>() {
            @Override
            public String getValue(Film film) {
                return Integer.toString(film.getRating()).concat("/10");
            }
        }, "Rating");

        cellTable.addColumn(new TextColumn<Film>() {
            @Override
            public String getValue(Film film) {
                return film.getComment();
            }
        }, "Comment");

        cellTable.addColumn(new Column<Film, String>(new ButtonCell() {
            @Override
            public void render(Context context, SafeHtml data, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant("<button type=\"button\" style=\"background-color: transparent;border: none;\" tabindex=\"-1\">");
                if (data != null) {
                    sb.append(data);
                }

                sb.appendHtmlConstant("</button>");
            }
        }) {
            @Override
            public String getValue(Film film) {
                return "X";
            }


            @Override
            public void onBrowserEvent(Cell.Context context, Element elem, Film object, NativeEvent event) {
                filmService.deleteFilm(object.getId(), new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        Window.alert("Cannot connect to server!");
                    }

                    @Override
                    public void onSuccess(Void aVoid) {
                        listDataProvider.getList().remove(object);
                        names.remove(object.getName());
                    }


                });
            }
        }, "Delete");

        cellTable.addColumn(new Column<Film, String>(new ButtonCell() {
            @Override
            public void render(Context context, SafeHtml data, SafeHtmlBuilder sb) {
                sb.appendHtmlConstant("<button type=\"button\" style=\"background-color: transparent;border: none;\" tabindex=\"-1\">");
                if (data != null) {
                    sb.append(data);
                }

                sb.appendHtmlConstant("</button>");
            }
        }) {
            @Override
            public String getValue(Film film) {
                return "Y";
            }

            @Override
            public void onBrowserEvent(Cell.Context context, Element elem, Film object, NativeEvent event) {
                genDialogBox(object).center();
            }
        }, "Edit");


        cellTable.setWidth("1000px");
        listDataProvider.addDataDisplay(cellTable);
        RootPanel.get("gridHandler").add(cellTable);


        sortType.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                Window.alert("Sort type: ".concat(Integer.toString(sortType.getSelectedIndex())));
                filmService.sort(sortType.getSelectedIndex(), new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        Window.alert("Server isn't responding!");
                    }

                    @Override
                    public void onSuccess(Void aVoid) {
                        switch (sortType.getSelectedIndex()) {
                            case Film.NEWEST:
                                Window.alert("Change to newest");
                                comparator = NEWEST;
                                break;
                            case Film.OLDEST:
                                comparator = OlDEST;
                                Window.alert("Change to oldest");
                                break;
                            case Film.MOST_LIKED:
                                comparator = MOST_LIKED;
                                Window.alert("Change to most_liked");
                                break;
                            case Film.LEAST_LIKED:
                                comparator = LEAST_LIKED;
                                Window.alert("Change to least_liked");
                                break;
                            case Film.NAME_DOWN:
                                comparator = NAME_DOWN;
                                Window.alert("Change to name_down");
                                break;
                            case Film.NAME_UP:
                                comparator = NAME_UP;
                                Window.alert("Change to name_up");
                                break;

                        }

                        listDataProvider.getList().sort(comparator);
                    }
                });

            }
        });

        HorizontalPanel sort = new HorizontalPanel();
        sort.add(new Label("Sorting:   "));
        sort.add(sortType);

        RootPanel.get("sort").add(sort);

    }


    private DialogBox genDialogBox(Film film) {
        DialogBox dialogBox = new DialogBox();

        Grid grid = new Grid(4, 2);

        Label label1 = new Label("Enter the name of the film");
        TextBox filmName = new TextBox();

        Label label2 = new Label("Select rating");
        ListBox rating = new ListBox();
        for (int i = 0; i < 11; i++) {
            rating.addItem(Integer.toString(i));
        }

        Label label3 = new Label("Enter comment");
        TextArea comment = new TextArea();
        Label error = new Label();
        error.setStyleName("serverResponseLabelError");

        Button cancel = new Button("Cancel");
        Button confirm = new Button("Confirm");


        cancel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                error.setText("");
                filmName.setText("");
                comment.setText("");
                add.setEnabled(true);
                dialogBox.hide();
            }
        });

        if (film == null) {
            dialogBox.setText("Add menu.");
            confirm.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent clickEvent) {
                    error.setText("");
                    if (filmName.getText().equals("")) {
                        error.setText("You forgot to specify the film!");
                    } else {
                        if (!names.contains(filmName.getText())) {

                            Film film = new Film(filmName.getText(), rating.getSelectedIndex(), comment.getText().replace('\n', ' '));

                            filmService.newFilm(film, new AsyncCallback<Void>() {
                                @Override
                                public void onFailure(Throwable throwable) {
                                    error.setText("Server isn't responding!");
                                }

                                @Override
                                public void onSuccess(Void aVoid) {
                                    comment.setText("");
                                    filmName.setText("");

                                    int where_add = Collections.binarySearch(listDataProvider.getList(), film, comparator);
                                    if (where_add < 0) {
                                        where_add = -where_add - 1;
                                    }
                                    listDataProvider.getList().add(where_add, film);

                                    add.setEnabled(true);
                                    dialogBox.hide();
                                }
                            });
                        } else {
                            error.setText("Film with this name already exists!");
                        }

                    }
                }
            });
        } else {
            dialogBox.setText("Edit menu.");
            filmName.setText(film.getName());
            rating.setTabIndex(film.getRating());
            comment.setText(film.getComment());
            cancel.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent clickEvent) {
                    add.setEnabled(true);
                    dialogBox.hide();

                }
            });

            confirm.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent clickEvent) {
                    error.setText("");
                    if (filmName.getText().equals("")) {
                        error.setText("You forgot to specify the film!");
                    } else {
                        if (!names.contains(filmName.getText()) || filmName.getText().equals(film.getName())) {
                            Film newFilm = new Film(filmName.getText(), rating.getSelectedIndex(), comment.getText().replace('\n', ' '));
                            newFilm.setId(film.getId());

                            filmService.edit(newFilm, new AsyncCallback<Void>() {
                                @Override
                                public void onFailure(Throwable throwable) {
                                    error.setText("Server isn't responding!");
                                }

                                @Override
                                public void onSuccess(Void aVoid) {
                                    listDataProvider.getList().remove(film);
                                    int where_add = Collections.binarySearch(listDataProvider.getList(), newFilm, comparator);
                                    if (where_add < 0) {
                                        where_add = -where_add - 1;
                                    }
                                    listDataProvider.getList().add(where_add, newFilm);
                                    add.setEnabled(true);
                                    dialogBox.hide();
                                }
                            });
                        } else {
                            error.setText("Film with this name already exists!");
                        }
                    }
                }
            });
        }

        VerticalPanel panel = new VerticalPanel();
        panel.add(label3);
        panel.add(error);

        grid.setWidget(0, 0, label1);
        grid.setWidget(0, 1, filmName);
        grid.setWidget(1, 0, label2);
        grid.setWidget(1, 1, rating);
        grid.setWidget(2, 0, panel);
        grid.setWidget(2, 1, comment);
        grid.setWidget(3, 0, confirm);
        grid.setWidget(3, 1, cancel);


        dialogBox.setWidget(grid);
        return dialogBox;
    }
}

