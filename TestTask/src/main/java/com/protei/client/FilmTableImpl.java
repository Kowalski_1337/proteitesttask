package com.protei.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.protei.shared.Film;

import java.util.List;
import java.util.function.Consumer;

public class FilmTableImpl extends FlexTable implements FilmTable {

    private int rowCounter;
    private Consumer<Integer> delete;
    //private Consumer<Film> add;

    FilmTableImpl(Consumer<Integer> delete/*, Consumer<Film> add*/) {
        super();
        //this.add = add;
        this.delete = delete;
        setText(0, 0, "Название");
        setText(0, 1, "Рейтинг");
        setText(0, 2, "Краткий комментарий");
        setText(0, 3, "Удалить");
        setWidth("800px");
        rowCounter = 1;
    }

    private Button genDelButton(int index) {
        Button button = new Button("X");
        button.addStyleName("deleteButton");
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                delete.accept(index);
            }
        });
        return button;
    }


    @Override
    public void addFilm(Film film) {
        setText(rowCounter, 0, film.getName());
        setText(rowCounter, 1, Integer.toString(film.getRating()).concat("/10"));
        setText(rowCounter, 2, film.getComment());
        setWidget(rowCounter, 3, genDelButton(film.getId()));
        rowCounter++;
    }

    @Override
    public void deleteFilm(int id) {
        removeRow(id);
        rowCounter--;
    }

    @Override
    public void setFilm(int index, Film film) {

    }

    @Override
    public Film getFilm(int index) {
        return null;
    }

    @Override
    public List<Film> getContent() {
        return null;
    }


    @Override
    public void addFilms(List<Film> films) {
        films.forEach(this::addFilm);
    }
}
