package com.protei.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.protei.shared.Film;

import java.util.List;

/**
 * The async counterpart of <code>FilmService</code>.
 */
public interface FilmServiceAsync {

    void getOld(AsyncCallback<List<Film>> async);


    void newFilm(Film film, AsyncCallback<Void> async);

    void deleteFilm(int id, AsyncCallback<Void> async);

    void sort(int sortType, AsyncCallback<Void> async);

    void edit(Film film, AsyncCallback<Void> async);

    void getSortType(AsyncCallback<Integer> async);
}
