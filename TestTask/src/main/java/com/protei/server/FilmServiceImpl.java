package com.protei.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.protei.client.FilmService;
import com.protei.shared.Film;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class FilmServiceImpl extends RemoteServiceServlet implements
        FilmService {

    private int nextId = 0;

    @Override
    public void newFilm(Film film) throws IOException {
        film.setId(nextId++);
        if (new File("films.txt").exists()) {
            Files.write(Paths.get("films.txt"), film.toString().getBytes(), StandardOpenOption.APPEND);
        } else {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("films.txt"))) {
                writer.write(film.toString());
            }
        }
    }

    @Override
    public List<Film> getOld() throws IOException {
        List<Film> films;

        genFiles();
        try (BufferedReader readAll = new BufferedReader(new FileReader("films.txt"));
             BufferedReader deleted = new BufferedReader(new FileReader("deleted.txt"));
             BufferedReader edit = new BufferedReader(new FileReader("edit.txt"))) {
            Set<Integer> deletedSet = deleted.lines().map(Integer::parseInt).
                    collect(Collectors.toSet());

            HashMap<Integer, Film> map = new HashMap<>();
            edit.lines().map(Film::new).forEach(f -> {
                if (map.replace(f.getId(), f) == null) {
                    map.put(f.getId(), f);
                }
            });

            nextId = 0;
            films = readAll.lines().map(Film::new).filter(f -> !deletedSet.contains(f.getId())).map(f -> {
                if (map.containsKey(f.getId())) {
                    return map.get(f.getId());
                }
                return f;
            }).peek(f -> f.setId(nextId++)).collect(Collectors.toList());
        }

        try (BufferedReader sortType = new BufferedReader(new FileReader("sortType.txt"));
             BufferedWriter writer = new BufferedWriter(new FileWriter("films.txt"));
             BufferedWriter cleanDeleted = new BufferedWriter(new FileWriter("deleted.txt"));
             BufferedWriter cleanEdit = new BufferedWriter(new FileWriter("edit.txt"))) {
            films.forEach(f -> {
                try {
                    writer.write(f.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            films.sort(genComparator(Integer.parseInt(sortType.readLine())));
            return films;
        }
    }

    @Override
    public int getSortType() throws IOException {
        if (new File("sortType.txt").exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader("sortType.txt"))) {
                return Integer.parseInt(reader.readLine());
            }
        } else {
            return Film.NEWEST;
        }
    }

    @Override
    public void deleteFilm(int id) throws IOException {
        if (new File("deleted.txt").exists()) {
            Files.write(Paths.get("deleted.txt"), Integer.toString(id).getBytes(), StandardOpenOption.APPEND);
        } else {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("films.txt"))) {
                writer.write(id);
            }
        }
    }

    @Override
    public void sort(int sortType) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("sortType.txt"))) {
            writer.write(Integer.toString(sortType));
        }
    }

    @Override
    public void edit(Film film) throws IOException {
        if (new File("edit.txt").exists()) {
            Files.write(Paths.get("edit.txt"), film.toString().getBytes(), StandardOpenOption.APPEND);
        } else {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("edit.txt"))) {
                writer.write(film.toString());
            }
        }
    }

    private Comparator<Film> genComparator(int sortType) {

        switch (sortType) {
            case Film.OLDEST:
                return Comparator.comparing(Film::getId);
            case Film.NEWEST:
                return Comparator.comparing(Film::getId).reversed();
            case Film.LEAST_LIKED:
                return Comparator.comparing(Film::getRating);
            case Film.MOST_LIKED:
                return Comparator.comparing(Film::getRating).reversed();
            case Film.NAME_UP:
                return Comparator.comparing(Film::getName);
            default:
                return Comparator.comparing(Film::getName).reversed();

        }
    }

    private void genFiles() throws IOException {
        File films = new File("films.txt");
        File deleted = new File("deleted.txt");
        File edit = new File("edit.txt");
        File sortType = new File("sortType.txt");
        if (!films.exists()) {
            films.createNewFile();
        }
        if (!deleted.exists()) {
            deleted.createNewFile();
        }
        if (!edit.exists()) {
            edit.createNewFile();
        }
        if (!sortType.exists()) {
            sortType.createNewFile();
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("sortType.txt"))){
                writer.write("0");
            }
        }
    }


}
