package com.protei.shared;

import java.io.Serializable;
//import lombok.Data;

/**
 * Structure for store data about viewed film
 */
/*@Data*/
public class Film implements Serializable {

    public static final int NEWEST = 0;
    public static final int OLDEST = 1;
    public static final int MOST_LIKED = 2;
    public static final int LEAST_LIKED = 3;
    public static final int NAME_UP = 4;
    public static final int NAME_DOWN = 5;

    private int id;
    private String name;
    private int rating;
    private String comment;
    private static final String SPLITTER_REG = "-@-@-@-";//TODO solve problem with double SPLITTER
    private static final String SPLITTER = "-@-@-@-";

    public Film(String name, int rating, String comment) {
        this.name = name;
        this.rating = rating;
        this.comment = comment;
    }

    public Film(Film other) {
        this.name = other.name;
        this.id = other.id;
        this.rating = other.rating;
        this.comment = other.comment;
    }

    public Film(String content) {
        String[] parts = content.split(SPLITTER_REG);

        this.name = parts[0];
        try {
            this.rating = Integer.parseInt(parts[1]);
        } catch (NumberFormatException e) {
            System.err.println(parts[1]);
        }
        this.comment = parts[2];
        try {
            this.id = Integer.parseInt(parts[3]);
        } catch (NumberFormatException e){
            System.err.println(parts[3]);
        }
    }

    @SuppressWarnings("unused")
    public Film() {
    }


    public String getName() {
        return name;
    }

    public int getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return name + SPLITTER + rating + SPLITTER + comment + SPLITTER + id + SPLITTER + '\n';
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
